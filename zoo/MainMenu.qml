import QtQuick 2.0

Rectangle {

    width: mainWindow.width
    height: mainWindow.height
    id: menu

    //Pozadí menu
    Image {
        id: zoo
        source: "../images/ZOO non flag.png"
        opacity: 100
        anchors.centerIn: parent
        width: parent.width
        height: parent.height
    }

    //Tlačitko pro spuštění hry
    Image {
        states: [
            State {
                name: "playEnter"
                PropertyChanges {
                    target: playSelected
                    opacity: 100
                }
                PropertyChanges{
                    target: playToolTip
                    visible: true
                }
            },
            State {
                name: "playExit"
                PropertyChanges {
                    target: playSelected
                    opacity: 0
                }
                PropertyChanges{
                    target: playToolTip
                    visible: false
                }
            },
            State {
                name: "clickedPlay"
                PropertyChanges {
                    target: map
                    visible: true
                }
                PropertyChanges {
                    target: play
                    visible: false
                }
                PropertyChanges {
                    target: load
                    visible: false
                }
            }
        ]

        id: play
        source: "../images/play.png"
        opacity: 100
        height: 200
        width: 200
        x: (mainWindow.width/2)-250
        y: (mainWindow.height/2)-100
        Image {
            id: playSelected
            source: "../images/playSelected.png"
            opacity: 0
            anchors.centerIn: parent
            height: parent.height
            width: parent.width
        }

        ToolTip{
           id: playToolTip
           description: "Start a new game!"
           visible: false
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                play.state = "playEnter"
            }
            onExited: {
                play.state = "playExit"
            }
            onClicked: {
                play.state = "clickedPlay"
                game.newGame()
            }
        }
    }

    //Tlačitko pro načtení hry
    Image {
        states: [
            State {
                name: "loadEnter"
                PropertyChanges {
                    target: loadSelected
                    opacity: 100
                }
                PropertyChanges{
                    target: loadToolTip
                    visible: true
                }
            },
            State {
                name: "loadExit"
                PropertyChanges {
                    target: loadSelected
                    opacity: 0
                }
                PropertyChanges{
                    target: loadToolTip
                    visible: false
                }
            },
            State {
                name: "clickedLoad"
                PropertyChanges {
                    target: map
                    visible: true
                }
                PropertyChanges {
                    target: load
                    visible: false
                }
                PropertyChanges {
                    target: play
                    visible: false
                }
            }
        ]

        id: load
        source: "../images/load.png"
        opacity: 100
        height: 200
        width: 200
        x: (mainWindow.width/2)+50
        y: (mainWindow.height/2)-100
        Image {
            id: loadSelected
            source: "../images/loadSelected.png"
            opacity: 0
            anchors.centerIn: parent
            height: parent.height
            width: parent.width
        }

        ToolTip{
           id: loadToolTip
           description: "Load a game to pick up where you left off!"
           visible: false
        }

        MouseArea {
            id: loadMouseArea
            width: parent.width
            height: parent.height
            hoverEnabled: true
            enabled: true
            onEntered: {
                load.state = "loadEnter"
            }
            onExited: {
                load.state = "loadExit"
            }
            onClicked: {
                load.state = "clickedLoad"
                game.loadGame()
            }
        }
    }

    Rectangle {
        id: creditsHelper
        width: 400
        height: 200
        color: "transparent"
        anchors.left: play.left
        anchors.top: play.bottom
        Rectangle {
            id: creditsButton
            color: "green"
            border.color: "#606060"
            border.width: 10
            anchors.centerIn: parent
            width: 200
            height: 100
            Text {
                id: creditsButtonText
                text: "About Game"
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
            }
        }
        MouseArea{
            anchors.fill: creditsButton
            hoverEnabled: true
            onEntered: {
                creditsButton.color = "white"
                creditsButtonText.color = "green"
            }
            onExited: {
                creditsButton.color = "green"
                creditsButtonText.color = "white"
            }
            onClicked: {
                gameCredits.visible = true
            }
        }
    }

    Credits {
        id: gameCredits
        visible: false
    }

}
