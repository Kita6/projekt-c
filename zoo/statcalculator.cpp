#include "statcalculator.h"

statCalculator::statCalculator()
{

}

// true => happy, false => unhappy
bool statCalculator::calculateHappiness(const std::vector<int> fields, const std::vector<int> levels){
    if(fields.size() == 0){
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("statCalculator, calculateHappiness: Vector is empty.");
        errors->printErrorsIntoFile();
    }

    int visitors = calculateVisitors(fields, levels);
    int max_visitors = calculateMaxVisitors(fields);
    if(visitors > max_visitors){
        return false;
    } else{
        return true;
    }
}

int statCalculator::calculateMaintanance(const std::vector<int> fields){
    int m_maintanance = 0;

    if(fields.size() == 0){
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("statCalculator, calculateMaintanance: Vector is empty.");
        errors->printErrorsIntoFile();
    }

    for (int i = 0; i < fields.size(); i++){
        if (fields.at(i) > 9 and fields.at(i) < 20){
            m_maintanance = m_maintanance + 30;
        } else if (fields.at(i) == 20){
            m_maintanance = m_maintanance + 20;
        } else if (fields.at(i) == 30){
            m_maintanance = m_maintanance + 40;
        } else if (fields.at(i) == 40){
            m_maintanance = m_maintanance + 60;
        } else if (fields.at(i) == 50){
            m_maintanance = m_maintanance + 80;
        }
    }
    return m_maintanance;
}

int statCalculator::calculateAllIncome(const std::vector<int> fields, const std::vector<int> levels){
    int m_income = calculateVisitors(fields, levels) * 20;
    return m_income;
}

int statCalculator::calculateVisitors(const std::vector<int> fields, const std::vector<int> levels){
    int m_visitors = 0;

    if(fields.size() == 0){
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("statCalculator, calculateVisitors: Vector is empty.");
        errors->printErrorsIntoFile();
    }

    for (int i =0; i < fields.size(); i++){
        if (fields.at(i) == 20){
            m_visitors = m_visitors + (levels.at(i)*3);
        } else if (fields.at(i) == 30){
            m_visitors = m_visitors + (levels.at(i)*4);
        } else if (fields.at(i) == 40){
            m_visitors = m_visitors + (levels.at(i)*5);
        } else if (fields.at(i) == 50){
            m_visitors = m_visitors + (levels.at(i)*6);
        }
    }
    return m_visitors;
}

int statCalculator::calculateMoneyPerTurn(const std::vector<int> fields, const std::vector<int> levels){
    if(fields.size() == 0){
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("statCalculator, calculateMoneyPerTurn: Vector is empty.");
        errors->printErrorsIntoFile();
    }

    int m_money = calculateAllIncome(fields, levels) - calculateMaintanance(fields);
    if(calculateHappiness(fields, levels) == false){
        m_money -= 0.1 * m_money;
    }
    return m_money;
}

int statCalculator::calculateMaxVisitors(const std::vector<int> fields){
    if(fields.size() == 0){
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("statCalculator, calculateMaxVisitors: Vector is empty.");
        errors->printErrorsIntoFile();
    }

    int m_maxVisitors = 20;
    for (int i =0; i < fields.size(); i++){
        if (fields.at(i) == 10){
            m_maxVisitors = m_maxVisitors + 20;
        } else if (fields.at(i) == 11) {
            m_maxVisitors = m_maxVisitors + 40;
        } else if (fields.at(i) == 12) {
            m_maxVisitors = m_maxVisitors + 60;
        } else if (fields.at(i) == 13) {
            m_maxVisitors = m_maxVisitors + 80;
        } else if (fields.at(i) == 14) {
           m_maxVisitors = m_maxVisitors + 100;
        }
    }
    return m_maxVisitors;
}

//CheckEnding metody se zavolaji pri next turn
bool statCalculator::checkEmuEnding(const std::vector<int> fields){
    if(fields.size() == 0){
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("statCalculator, checkEmuEnding: Vector is empty.");
        errors->printErrorsIntoFile();
    }

    int m_numberOfEmus = 0;
    for (int i = 0; i < fields.size(); i++){
        if (fields.at(i) == 20){
            m_numberOfEmus++;
        }
    }
    if (m_numberOfEmus > 4){
        return true;
    } else return false;
}

bool statCalculator::checkLamaEnding(const std::vector<int> fields){
    if(fields.size() == 0){
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("statCalculator, checkLamaEnding: Vector is empty.");
        errors->printErrorsIntoFile();
    }

    int m_numberOfMajesties = 0;
    for (int i = 0; i < fields.size(); i++){
        if (fields.at(i) == 40){
            m_numberOfMajesties++;
        }
    }
    if (m_numberOfMajesties > 4){
        return true;
    } else return false;
}

bool statCalculator::checkBrokeEnding(const int money, const int unhappyTurns){
    if ((money < -499) or (unhappyTurns > 3)){
        return true;
    } else return false;
}

bool statCalculator::checkVictory(const std::vector<int> levels){
    int m_totalLevels;
    for (int i = 0; i < levels.size(); i++){
        m_totalLevels = m_totalLevels + levels.at(i);
    }
    if (m_totalLevels >= 75){
        return true;
    } else return false;
}
