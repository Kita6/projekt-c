#include "filemanager.h"

FileManager::FileManager()
{

}

void FileManager::loadGame(std::vector<int>& field,std::vector<int>& level, int& money, int& actions, int& turns){
    QFile file (":/Saving.xml");
    if(file.open(QIODevice::ReadOnly)){
        QXmlStreamReader xmlReader;
        xmlReader.setDevice(&file);
        xmlReader.readNext();
        while(!xmlReader.isEndDocument()){
            if (xmlReader.isStartElement()){
                QString name = xmlReader.name().toString();
                if(name == "field"){
                    int index = xmlReader.attributes().value("index").toInt();
                    int zooCount = xmlReader.readElementText().toInt();
                    field.at(index) = zooCount;
                } else if (name == "zoo"){
                    // skip
                }else if (name == "money"){
                    money = xmlReader.readElementText().toInt();
                } else if(name == "actions"){
                    actions = xmlReader.readElementText().toInt();
                } else if(name == "turns"){
                    turns = xmlReader.readElementText().toInt();
                } else if (name == "level"){
                    int index = xmlReader.attributes().value("index").toInt();
                    int levelCount = xmlReader.readElementText().toInt();
                    level.at(index) = levelCount;
                }
                else{
                    qCritical() << "Narazil jsem na element jiny nez field" << name <<"\n";
                    errorManager* errors = errorManager::getErrorManager();
                    errors->addError("fileManager, loadGame: There is another element.");
                    errors->printErrorsIntoFile();
                }
            }
            xmlReader.readNext();
        }
    } else{
        qCritical() << "The file cannot be opened. \n";
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("fileManager, loadGame: File cannot be opened.");
        errors->printErrorsIntoFile();
    }
}

void FileManager::saveXml(const std::vector<int> save, const std::vector<int> level, const int money, const int actions, const int turns){ // zkouska ukladani je v zoogame.cpp
    std::vector<QString> m_convert;

    for(int i = 0; i < 16; i++){
        QString numberConverted = QString::number(save.at(i));
        m_convert.push_back(numberConverted);
    }

    std::vector<QString> m_levelConvert;

    for(int i = 0; i < 16; i++){
        QString numberConverted1 = QString::number(level.at(i));
        m_levelConvert.push_back(numberConverted1);
    }

    //QString fileName = "D:/Downloads/Projekt Local/31.12.2021/zoo/Saving.xml";
    QString fileName = "C:/C++ Projekt/projekt-c/zoo/Saving.xml";
    QFile file(fileName);

    if(file.open(QIODevice::WriteOnly)){
        QXmlStreamWriter writer;
        writer.setDevice(&file);

        writer.writeStartDocument();  // zacatek dokumentu
        writer.writeStartElement("zoo"); // pocatecni element - zoo

        //Ukládá typ budovy
        for(int i = 0; i < 16; i++){
            writer.writeStartElement("field");
            writer.writeAttribute("index", QString::number(i));
            writer.writeCharacters(m_convert.at(i));
            writer.writeEndElement();
        }

        //Ukládá level budovy
        for(int i = 0; i < 16; i++){
            writer.writeStartElement("level");
            writer.writeAttribute("index", QString::number(i));
            writer.writeCharacters(m_levelConvert.at(i));
            writer.writeEndElement();
        }

        writer.writeStartElement("money");
        writer.writeCharacters(QString::number(money));
        writer.writeEndElement();

        writer.writeStartElement("actions");
        writer.writeCharacters(QString::number(actions));
        writer.writeEndElement();

        writer.writeStartElement("turns");
        writer.writeCharacters(QString::number(turns));
        writer.writeEndElement();

        writer.writeEndElement();
        writer.writeEndDocument();  // konec dokumentu

        file.close();
    } else{
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("fileManager, saveXml: File cannot be opened.");
        errors->printErrorsIntoFile();
        qCritical() << "The file cannot be opened. \n";

    }


}
