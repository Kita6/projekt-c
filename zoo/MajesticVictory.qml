import QtQuick 2.0

Rectangle {
    property alias majesticCountdown: timer1.running
    width: parent.width
    height: parent.height
    Image {
        id: background
        source: "../images/ZOO non flagBW.png"
        width: parent.width
        height: parent.height
    }
    Rectangle {
        id: blackBar
        width: parent.width
        height: 130
        anchors.centerIn: parent
        color: "black"
        opacity: 80
    }
    Text{
        id: greatStoryTelling1
        anchors.centerIn: parent
        text: "You always knew that the only way humanity can become better is by following in llama's footsteps."
        font.pixelSize: 30
        color: "green"
        opacity: 100
        Connections {
           target: game
           function onSpittyVictory(){
               timer1.running = true
           }
        }
        Timer {
            id: timer1
            interval: 5000
            running: false
            onTriggered: {
                greatStoryTelling1.visible = false
                greatStoryTelling2.visible = true
                timer2.running = true
            }
        }
    }
    Text {
        id: greatStoryTelling2
        anchors.centerIn: parent
        text: "Because there aren't any creatures more majestic."
        font.pixelSize: 30
        color: "green"
        visible: false
        Timer {
            id: timer2
            interval: 5000
            running: false
            onTriggered: {
                greatStoryTelling2.visible = false
                greatStoryTelling3.visible = true
                timer3.running = true
            }
        }
    }
    Text {
        id: greatStoryTelling3
        anchors.centerIn: parent
        text: "You did everything in your power to share these beautiful creatures with the rest of mankind."
        font.pixelSize: 30
        color: "green"
        visible: false
        Timer {
            id: timer3
            interval: 7000
            running: false
            onTriggered: {
                greatStoryTelling3.visible = false
                greatStoryTelling4.visible = true
                timer4.running = true
            }
        }
    }
    Text {
        id: greatStoryTelling4
        anchors.centerIn: parent
        text: "With your help, every visitor of this ZOO has seen the light."
        font.pixelSize: 30
        color: "green"
        visible: false
        Timer {
            id: timer4
            interval: 5000
            running: false
            onTriggered: {
                greatStoryTelling4.visible = false
                greatStoryTelling5.visible = true
                timer5.running = true
            }
        }
    }
    Text {
        id: greatStoryTelling5
        anchors.centerIn: parent
        text: "And llamas became what they were always destined to be..."
        font.pixelSize: 30
        color: "green"
        visible: false
        Timer {
            id: timer5
            interval: 5000
            running: false
            onTriggered: {
                greatStoryTelling5.visible = false
                greatStoryTelling6.visible = true
                timer6.running = true
            }
        }
    }
    Text {
        id: greatStoryTelling6
        anchors.centerIn: parent
        text: "Kings and Queens."
        font.pixelSize: 30
        color: "green"
        visible: false
        Timer {
            id: timer6
            interval: 5000
            running: false
            onTriggered: {
                greatStoryTelling6.visible = false
                greatStoryTelling7.visible = true
                timer7.running = true
            }
        }
    }
    Rectangle {
        id: greatStoryTelling7
        visible: false
        color: "black"
        width: parent.width
        height: parent.height
        Text {
            anchors.centerIn: parent
            text: "VICTORY!"
            font.pixelSize: 80
            color: "green"
            Timer {
                id: timer7
                interval: 2000
                running: false
                onTriggered: {
                    greatStoryTelling7.visible = false
                    greatStoryTelling8.visible = true
                    blackBar.visible = false
                }
            }
        }
    }
    Image {
        id: greatStoryTelling8
        source: "../images/Majestic Victory.png"
        width: parent.width
        height: parent.height
        visible: false
    }
}

