import QtQuick 2.0

Rectangle {
    width: 150
    height: parent.height
    color: "transparent"
    anchors.right: parent.right

    Image{

        states: [
            State {
                name: "saveEntered"
                PropertyChanges {
                    target: saveSelected
                    opacity: 100
                }
                PropertyChanges{
                    target: saveToolTip
                    visible: true
                }

            },
            State {
                name: "saveExit"
                PropertyChanges {
                    target: saveSelected
                    opacity: 0
                }
                PropertyChanges{
                    target: saveToolTip
                    visible: false
                }
            }
        ]

        id: save
        source: "../images/save.png"
        anchors.centerIn: parent
        width: 100
        height: 100

        Image {
            id: saveSelected
            source: "../images/saveSelected.png"
            opacity: 0
            anchors.centerIn: parent
            width: parent.width
            height: parent.height
        }

        ToolTip{
            id: saveToolTip
            description: "Save your game to\ncontinue later!"
            visible: false
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                save.state = "saveEntered"
            }

            onExited: {
                save.state = "saveExit"
            }
            onClicked: {
                game.saveGame()
                saveDialog.open()
            }
        }
    }


    Image{

        states: [
            State {
                name: "turnEntered"
                PropertyChanges {
                    target: turnSelected
                    opacity: 100
                }
                PropertyChanges{
                    target: nextTurnToolTip
                    visible: true
                }
            },
            State {
                name: "turnExit"
                PropertyChanges {
                    target: turnSelected
                    opacity: 0
                }
                PropertyChanges{
                    target: nextTurnToolTip
                    visible: false
                }
            }
        ]

        id: turn
        source: "../images/nextTurn.png"
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: 200
        height: 200

        Image {
            id: turnSelected
            source: "../images/nextTurnSelected.png"
            opacity: 0
            anchors.centerIn: parent
            width: parent.width
            height: parent.height
        }

        ToolTip{
            id: nextTurnToolTip
            visible: false
            description: "End your turn."
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                turn.state = "turnEntered"
            }

            onExited: {
                turn.state = "turnExit"
            }
            onClicked: {
                game.nextTurn();
            }
        }
    }
}
