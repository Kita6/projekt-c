import QtQuick 2.0

Rectangle{
    id: credits
    width: mainWindow.width
    height: mainWindow.height
    color: "black"
    visible: false

    Text{
        text: "PEF MENDELU\n\nProgramovací jazyk C++\n\nSemestrální projekt - zimní semestr 2021/2022\n\nKristýna Klimešová\n\nZdeněk Šilhán\n\nTomáš Přikryl\n\nSpeciální poděkování našim betatesterům:\n\nOndřej Bečva"
        color: "white"
        anchors.centerIn: parent
        font.pointSize: 20
        font.bold: true
    }

    Rectangle {
        id: creditsCloseButton
        color: "transparent"
        anchors.top: credits.top
        anchors.right: credits.right
        width: 100
        height: 100

        states: [
            State {
                name: "creditsCloseButtonEntered"
                PropertyChanges {
                    target: creditsButtonText
                    color: "red"
                }
            },
            State {
                name: "creditsCloseButtonExited"
            },
            State {
                name: "creditsCloseButtonClicked"
                PropertyChanges {
                    target: credits
                    visible: false
                }
            }
        ]

        Text {
            id: creditsButtonText
            text: "X"
            color: "white"
            font.pixelSize: 50
            anchors.centerIn: parent
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                creditsCloseButton.state = "creditsCloseButtonEntered"
            }
            onExited: {
                creditsCloseButton.state = "creditsCloseButtonExited"
            }
            onClicked: {
                creditsCloseButton.state = "creditsButtonClicked"
                credits.visible = false
            }
        }
    }
}
