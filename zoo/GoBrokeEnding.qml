import QtQuick 2.0

Rectangle {
        width: parent.width
        height: parent.height

        Image {
            id: background
            source: "../images/ZOO non flagBW.png"
            width: parent.width
            height: parent.height
        }

        Rectangle {
            id: blackBar
            width: parent.width
            height: 130
            anchors.centerIn: parent
            color: "black"
            opacity: 80
        }

        Text{
            id: greatStoryTelling1
            anchors.centerIn: parent
            text: "Running a ZOO was always your dream..."
            font.pixelSize: 30
            color: "red"
            opacity: 100

            Connections {
               target: game
               function onINeedMoney(){
                   timer1.running = true
               }
            }

            Timer {
                id: timer1
                interval: 3000
                running: false
                onTriggered: {
                    greatStoryTelling1.visible = false
                    greatStoryTelling2.visible = true
                    timer2.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling2
            anchors.centerIn: parent
            text: "...and for a while it was more than just a dream."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer2
                interval: 3000
                running: false
                onTriggered: {
                    greatStoryTelling2.visible = false
                    greatStoryTelling3.visible = true
                    timer3.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling3
            anchors.centerIn: parent
            text: "But after many turns of financial hardships and unhappy visitors..."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer3
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling3.visible = false
                    greatStoryTelling4.visible = true
                    timer4.running = true
                }
           }
        }

        Text {
            id: greatStoryTelling4
            anchors.centerIn: parent
            text: "...you had no choice but to step down as ZOO's manager."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer4
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling4.visible = false
                    greatStoryTelling5.visible = true
                    timer5.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling5
            anchors.centerIn: parent
            text: "All you can do now is hope that someone else will continue your dream."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer5
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling5.visible = false
                    greatStoryTelling6.visible = true
                    timer6.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling6
            anchors.centerIn: parent
            text: "At the very least you can sleep easy knowing that there was no Emu rebellion..."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer6
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling6.visible = false
                    greatStoryTelling7.visible = true
                }
            }
        }

        Rectangle {
        id: greatStoryTelling7
        width: parent.width
        height: parent.height
        color: "black"
        visible: false
            Text {
                anchors.centerIn: parent
                text: "GAME OVER"
                font.pixelSize: 80
                color: "red"
            }
       }
    }

