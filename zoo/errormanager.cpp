#include "errormanager.h"

// pokud by nenastal zadny error, instance errorManageru je nullptr
errorManager* errorManager::s_error = nullptr;

errorManager::errorManager(){
    printErrorsIntoFile();
}


errorManager* errorManager::getErrorManager(){
    if(s_error == nullptr){
        s_error = new errorManager();
    }
    return s_error;
}

void errorManager::addError(QString error){
    m_errors.push_back(error);
}


void errorManager::printErrorsIntoFile(){
    //QString fileName = "D:/Downloads/Projekt Local/8.12/projekt-c/zoo/Errors.xml";
    //QString fileName = "D:/Downloads/Projekt Local/31.12.2021/zoo/Errors.xml";
    QString fileName = "C:/C++ Projekt/projekt-c/zoo/Errors.xml";
    QFile file(fileName);

    if(file.open(QIODevice::WriteOnly)){
        QXmlStreamWriter writer;
        writer.setDevice(&file);

        writer.writeStartDocument();  // zacatek dokumentu
        writer.writeStartElement("errors"); // pocatecni element - Errors

        if(m_errors.size() != 0){
            for(auto e: m_errors){
                writer.writeStartElement("error");
                writer.writeCharacters(e);
                writer.writeEndElement();
            }
        }

        writer.writeEndElement();
        writer.writeEndDocument();  // konec dokumentu
        file.close();
    } else {
        qCritical() << "The file cannot be opened. \n";
        errorManager* errors = errorManager::getErrorManager();
        errors->addError("errorManager, printErrorsIntoFile: File cannot be opened.");
        errors->printErrorsIntoFile();
    }

}
