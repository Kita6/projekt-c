#ifndef STATCALCULATOR_H
#define STATCALCULATOR_H
#include <vector>
#include "zoogame.h"
#include "errormanager.h"

class statCalculator
{
public:
    statCalculator();
    static bool calculateHappiness(const std::vector<int> fields, const std::vector<int> levels);
    static int calculateMaintanance(const std::vector<int> fields);
    static int calculateAllIncome(const std::vector<int> fields, const std::vector<int> levels);
    static int calculateVisitors(const std::vector<int> fields, const std::vector<int> levels);
    static int calculateMoneyPerTurn(const std::vector<int> fields, const std::vector<int> levels);
    static int calculateMaxVisitors(const std::vector<int> fields);
    static bool checkEmuEnding(const std::vector<int> fields);
    static bool checkLamaEnding(const std::vector<int> fields);
    static bool checkBrokeEnding(const int money, const int unhappyTurns);
    static bool checkVictory(const std::vector<int> levels);
};

#endif // STATCALCULATOR_H
