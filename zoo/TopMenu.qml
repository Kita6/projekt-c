import QtQuick 2.0

Rectangle {
    color: "black"
    width: parent.width
    height: 50
    Text {
        id: topText
        text: "Money: $SAMPLE Visitors: SAMPLE/SAMPLE Turn: SAMPLE"
        color: "white"
        font.pixelSize: 30
        anchors.centerIn: parent
        Connections{
            target: game
            function onTopChange(){
                topText.text = game.getStats()
            }
        }
    }
}
