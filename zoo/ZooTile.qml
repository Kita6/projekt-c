import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.15

Rectangle {
    id: zooTile
    width: 200;
    height: 200;
    color: "gray"

    property alias tileIndex: label.text  // indexy policek

    Text {
        id: label
        text: "0"
        visible: false
        color: "white"
    }

    states: [
        State {
            name: "emu"
            PropertyChanges {target: emu; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
            PropertyChanges {target: menuEmu; enabled: true}
        },
       State {
            name: "lama"
            PropertyChanges {target: lama; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
            PropertyChanges {target: menuLama; enabled: true}
        },
        State {
        name: "fox"
            PropertyChanges {target: fox; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
            PropertyChanges {target: menuFox; enabled: true}
        },
        State {
            name: "okapi"
            PropertyChanges {target: okapi; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
            PropertyChanges {target: menuOkapi; enabled: true}
        },
        State {
            name: "wc"
            PropertyChanges {target: wc; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
            PropertyChanges {target: menuWc; enabled: true}
        },
        State {
            name: "iceCream"
            PropertyChanges {target: iceCream; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
            PropertyChanges {target: wc; visible: false }
            PropertyChanges {target: menuIce; enabled: true}
        },
        State {
            name: "buffet"
            PropertyChanges {target: buffet; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
            PropertyChanges {target: iceCream; visible: false }
            PropertyChanges {target: menuBuffet; enabled: true}
        },
        State {
            name: "gift"
            PropertyChanges {target: gift; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
            PropertyChanges {target: buffet; visible: false }
            PropertyChanges {target: menuGift; enabled: true}
        },
        State {
            name: "hotel"
            PropertyChanges {target: hotel; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
            PropertyChanges {target: gift; visible: false }
            PropertyChanges {target: menuHotel; enabled: true}
        },
        State {
            name: "main"
            PropertyChanges {target: mainBuilding; visible: true }
            PropertyChanges {target: addBuilding; visible: false }
            PropertyChanges {target: level; visible: true }
        }
    ]

    Image{
        states: [
            State {
                name: "menuOpened"
                PropertyChanges {target: buildingMenu; visible: true }
            }
        ]
        id: addBuilding
        source: "../images/addBuilding.png"
        anchors.centerIn: parent
        width: 100
        height: 100
        visible: true

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
               addBuilding.state = "addBuildingEnter"
            }
            onClicked: {
                addBuilding.state = "menuOpened"
                game.getIndex(index)
            }
        }

        Connections{
            target: game
            function onVectorChange(vector){
                game.getIndex(index)
                var zooCount = game.getZooCount(tileIndex); // v promenne zooCount je cislo, ktere bude ulozeno na prislunem indexu
                if(zooCount === 1){
                    zooTile.state = "main"
                } if(zooCount === 10){
                    zooTile.state = "wc"}
                if(zooCount === 11){
                    zooTile.state = "iceCream"
                }
                if(zooCount === 12){
                    zooTile.state = "buffet"
                }
                if(zooCount === 13){
                    zooTile.state = "gift"
                }
                if(zooCount === 14){
                    zooTile.state = "hotel"
                }
                if(zooCount === 20){
                    zooTile.state = "emu"                 
                }
                if(zooCount === 30){
                    zooTile.state = "fox"
                }
                if(zooCount ===40){
                    zooTile.state = "lama"
                }
                if(zooCount === 50){
                    zooTile.state = "okapi"
                }
            }
        }
    }

    Image {
       visible: false
       id: emu
       width: parent.width-10
       height: parent.height -10
       source: "../images/Animals/VybehEmu.png"
       anchors.centerIn: parent
       UpgradeMenu {
        id: menuEmu
        enabled: false
       }
    }

    Image {
       id: fox
       visible: false
       width: parent.width-10
       height: parent.height -10
       source: "../images/Animals/VybehLiska.png"
       anchors.centerIn: parent
       UpgradeMenu {
           id: menuFox
           enabled: false
       }
    }

    Image {
       id: okapi
       visible: false
       width: parent.width-10
       height: parent.height -10
       source: "../images/Animals/VybehOkapi.png"
       anchors.centerIn: parent
       UpgradeMenu {
           id: menuOkapi
           enabled: false
       }
    }

    Image{
        id: lama
        visible: false
        width: parent.width-10
        height: parent.height -10
        source: "../images/Animals/VybehLama.png"
       anchors.centerIn: parent
       UpgradeMenu {
           id: menuLama
           enabled: false
       }
    }

    Image {
        id: wc
        visible: false
        width: parent.width-10
        height: parent.height -10
        source: "../images/BudovaWC.png"
        anchors.centerIn: parent
        UpgradeMenu {
            id: menuWc
            enabled: false
        }
    }

    Image {
        id: iceCream
        visible: false
        width: parent.width-10
        height: parent.height -10
        source: "../images/BudovaZmrlina.png"
        anchors.centerIn: parent
        UpgradeMenu {
            id: menuIce
            enabled: false
        }
    }

    Image {
        id: buffet
        visible: false
        width: parent.width-10
        height: parent.height -10
        source: "../images/BudovaBufet.png"
        anchors.centerIn: parent
        UpgradeMenu {
            id: menuBuffet
            enabled: false
        }
    }

    Image {
        id: gift
        visible: false
        width: parent.width-10
        height: parent.height -10
        source: "../images/BudovaGift.png"
        anchors.centerIn: parent
        UpgradeMenu {
            id: menuGift
            enabled: false
        }
    }

    Image {
        id: hotel
        visible: false
        width: parent.width-10
        height: parent.height -10
        source: "../images/BudovaHotel.png"
        anchors.centerIn: parent
        UpgradeMenu {
            id: menuHotel
            enabled: false
        }
    }

    Image {
        id: mainBuilding
        visible: false
        width: parent.width-10
        height: parent.height -10
        source: "../images/BudovaZakladni.png"
        anchors.centerIn: parent
        StatsMenu {
        }
    }

    Rectangle {
        id: level
        visible: false
        anchors.top: parent.top
        anchors.right: parent.right
        width: 50
        height: 50
        border.color: "black"
        border.width: 3
        color: "gray"
        Text {
            id: levelState
            text: "0"
            anchors.centerIn: parent
            font.pointSize: 30
            Connections{
                target: game
                function onLevelChange(vector){
                    game.getIndex(index)
                    levelState.text = game.getLevel(tileIndex); // v promenne zooCount je cislo, ktere bude ulozeno na prislunem indexu
                }
           }
       }
    }
}
