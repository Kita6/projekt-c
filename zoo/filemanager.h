#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFile>
#include <QDebug>
#include <vector>
#include <QString>
#include "zoogame.h"
#include "errormanager.h"

class FileManager
{
public:
    FileManager();
    static void loadGame(std::vector<int>& field,std::vector<int>& level, int& money, int& actions, int& turns);
    static void saveXml(const std::vector<int> save, const std::vector<int> level, const int money, const int actions, const int turns); // ulozeni do souboru
};

#endif // FILEMANAGER_H
