import QtQuick 2.0

Rectangle{
    id: buildingMenu
    width: mainWindow.width/1.5
    height: mainWindow.height/1.5
    anchors.centerIn: parent
    color: "white"




    Image{
        source: "../images/buildingMenu.png"
        width: mainWindow.width/1.5
        height: mainWindow.height/1.5
        anchors.centerIn: parent



        // tlačítko pro postavení vybehu s emu - silueta
       Rectangle {
           id: buildingMenuBuildEmu
           width: 200
           height: 200
           color: "white"
           anchors.horizontalCenter: parent.horizontalCenter
           anchors.top: parent.top


           Image{
               source: "../images/IconEmu.png"
               width: parent.width
               height: parent.height
           }

           MouseArea {
               anchors.fill: parent
               onClicked: {
                   buildingMenu.visible = false


               }
           }
       }


    }
}
