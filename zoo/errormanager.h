#ifndef ERRORMANAGER_H
#define ERRORMANAGER_H
#include <QString>
#include "error.h"
#include <vector>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFile>
#include <QDebug>


//Pokud nastane chyba, vytiskne se do souboru. (napr. spatne otevreny soubor)
class errorManager
{
    static errorManager* s_error;
    std::vector<QString> m_errors;

    errorManager();

public:
    static errorManager* getErrorManager();
    void addError(QString error);
    void printErrorsIntoFile();
};

#endif // ERRORMANAGER_H
