import QtQuick 2.0

Rectangle{
    property string description
    width: childrenRect.width+10
    height: childrenRect.height+10
    anchors.centerIn: parent
    visible: false
    border.width: 1
    border.color: "black"
    Text{text: description; anchors.centerIn: parent }
}
