import QtQuick 2.0

Image{
    id: statsMenu
    states: [
        State {
            name: "statsMenuOpened"
            PropertyChanges { target: statsMenu; opacity: 100 }
        },
        State {
            name: "statsMenuClosed"
            PropertyChanges { target: statsMenu; opacity: 0 }
        }

    ]
    source: "../images/buildingMenu.png"
    opacity: 0
    width: parent.height-10
    height: parent.width-10
    anchors.centerIn: parent

    Rectangle{
        id: closeButton
        anchors.left: parent.left
        anchors.top: parent.top
        width: 50
        height: 50
        color: "transparent"
        Text {
            id: closeButtonText
            text: "X"
            color: "black"
            font.pixelSize: 30
            anchors.centerIn: parent
            states: [
                State {
                    name: "statsCloseButtonEntered"
                    PropertyChanges {
                        target: closeButtonText
                        color: "red"
                    }
                },
                State {
                    name: "statsCloseButtonExited"
                    PropertyChanges{
                        target: closeButtonText
                        color: "black"
                    }
                }
            ]

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    closeButtonText.state = "statsCloseButtonEntered"
                }
                onExited: {
                    closeButtonText.state = "statsCloseButtonExited"
                }
                onClicked: {
                    statsMenu.state = "statsMenuClosed"
                }
            }
        }
    }

    MouseArea{
        height: 100
        width: 100
        anchors.centerIn: statsMenu
        onClicked: {
            game.getIndex(index)
            statsMenu.state = "statsMenuOpened"
        }
    }

    Text {
        id: stats
        text: "I AM DEFAULT TEXT"
        font.pointSize: 10
        anchors.centerIn: parent
        Connections {
        target: game
            function onTopChange(){
                stats.text = game.getMoreStats()
            }
        }
    }
}
