import QtQuick 2.0

Image{
    id: upgradeMenu
    states: [
        State {
            name: "menuOpened"
            PropertyChanges { target: upgradeMenu; opacity: 100 }
        },
        State {
            name: "menuClosed"
            PropertyChanges { target: upgradeMenu; opacity: 0 }
        }

    ]
    source: "../images/buildingMenu.png"
    opacity: 0
    width: parent.height-10
    height: parent.width-10
    anchors.centerIn: parent

    onOpacityChanged: {
        var currentLevel = game.getLevel();
        if (currentLevel < 5) {
            upgradeButton.state = "upgradePossibleR"
            price.state = "upgradePossible"
        } else {
            upgradeButton.state = "upgradeShallNotPassR"
            price.state = "upgradeShallNotPass"
        }
    }

    Rectangle{
        id: closeButton
        anchors.left: parent.left
        anchors.top: parent.top
        width: 50
        height: 50
        color: "transparent"
        Text {
            id: closeButtonText
            text: "X"
            color: "black"
            font.pixelSize: 30
            anchors.centerIn: parent
            states: [
                State {
                    name: "closeButtonEntered"
                    PropertyChanges {
                        target: closeButtonText
                        color: "red"
                    }
                },
                State {
                    name: "closeButtonExited"
                    PropertyChanges{
                        target: closeButtonText
                        color: "black"
                    }
                }
            ]

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    closeButtonText.state = "closeButtonEntered"
                }
                onExited: {
                    closeButtonText.state = "closeButtonExited"
                }
                onClicked: {
                    upgradeMenu.state = "menuClosed"
                }
            }
        }
    }

    MouseArea{
        height: 100
        width: 100
        anchors.centerIn: upgradeMenu
        onClicked: {
            game.getIndex(index)
            upgradeMenu.state = "menuOpened"
        }
    }

    Text {
        id: price
        text: "$300"
        font.pointSize: 20
        anchors.bottom: upgradeButton.top
        anchors.horizontalCenter: upgradeButton.horizontalCenter
        states: [
            State{
                    name: "upgradePossible"
                    PropertyChanges {
                        target: price
                        text: "$300"
                    }
            },
            State {
              name: "upgradeShallNotPass"
              PropertyChanges {
                  target: price
                  text: "Max level"
              }
            }
        ]
    }

    Rectangle {
        id: upgradeButton
        width: parent.width-20
        height: 80
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        color: "green"
        border.color: "white"
        border.width: 10
        radius: 10
        anchors.margins: 10
        visible: true
        states: [
            State{
                    name: "upgradePossibleR"
                    PropertyChanges {
                        target: upgradeButton
                        visible: true
                    }
            },
            State {
              name: "upgradeShallNotPassR"
              PropertyChanges {
                  target: upgradeButton
                  visible: false
              }
            }
        ]


        Text {
            id: upgradeText
            text: "Upgrade!"
            color: "white"
            font.pointSize: 20
            anchors.centerIn: parent

            states: [
                State{
                        name: "upgradeHover"
                        PropertyChanges {
                            target: upgradeText
                            color: "red"
                        }
                },
                State {
                  name: "upgradeNotHover"
                  PropertyChanges {
                      target: upgradeText
                      color: "white"
                  }
                }
            ]

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    upgradeText.state = "upgradeHover"
                }
                onExited: {
                    upgradeText.state = "upgradeNotHover"
                }
                onClicked: {
                    upgradeMenu.state = "menuClosed"
                    game.upgradeLevel()
                }
            }
        }
    }
}
