import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    id: mainWindow
    width: 1920
    height: 1080
    visible: true
    title: qsTr("ZOO")

    MainMenu {

    }

    PlayingField {
        id: map
    }

    EmuEnding {
        id: emuRevolution
        visible: false
        Connections {
           target: game
           function onVivaLaRevolucion(){
               emuRevolution.visible = true
           }
        }
    }

    GoBrokeEnding {
        id: giveMeMoney
        visible: false
        Connections {
           target: game
           function onINeedMoney(){
               giveMeMoney.visible = true
           }
        }
    }

    MajesticVictory {
        id: spittingMajesty
        visible: false
        Connections{
            target: game
            function onSpittyVictory(){
                spittingMajesty.visible = true
            }
        }
    }

    ScoreVictory {
        id: testOfTime
        visible: false
        Connections {
            target: game
            function onLuckyManager(){
                testOfTime.visible = true
            }
        }
    }
}
