#include "error.h"

Error::Error(QString nazev, QString text){
    m_nazev = nazev;
    m_text = text;
}

QString Error::getNazev() const{
    return m_nazev;
}

QString Error::getText() const{
    return m_text;
}

void Error::setNazev(QString nazev){
    m_nazev = nazev;
}
void Error::setText(QString text){
    m_text = text;
}
