#ifndef ZOOGAME_H
#define ZOOGAME_H

#include <QObject>
#include <vector>
#include "filemanager.h"
#include "statcalculator.h"
#include "errormanager.h"

class ZooGame : public QObject
{
    Q_OBJECT
public:
    std::vector<int> m_zoo;
    std::vector<int> m_levels;
private:
    int m_index;
    int m_money;   // celkove penize
    int m_turns;
    QString m_stats;
    int m_actionsLeft;
    int m_goldPerTurn;
    int m_visitors;
    int m_maxVisitors;
    int m_unhappyTurns;
    // nespokojenost, ktera je na 0, pokud zakaznici prekroci kapacitu, tak se nespokojenost zvysi
public:
    explicit ZooGame(QObject *parent = nullptr);

    Q_INVOKABLE int getZooCount(int index);
    Q_INVOKABLE int getLevel(int index);// metoda ktera prebere index a vrati cislo, ktere se na indexu nachazi
    Q_INVOKABLE void saveIntoTile(int id, int price);
    Q_INVOKABLE void getIndex(int index);
    Q_INVOKABLE void newGame();
    Q_INVOKABLE void loadGame();
    Q_INVOKABLE void saveGame();
    Q_INVOKABLE bool isEmpty();
    Q_INVOKABLE QString getStats();
    Q_INVOKABLE void nextTurn();
    Q_INVOKABLE void upgradeLevel();
    Q_INVOKABLE QString getMoreStats();

    // zakladni get Metody:
    Q_INVOKABLE int getMoney();
    Q_INVOKABLE int getTurns();
    Q_INVOKABLE int getActionsLeft();
    Q_INVOKABLE int getLevel();

signals:
    void vectorChange(const std::vector<int> vector);
    void topChange();
    void vivaLaRevolucion();
    void iNeedMoney();
    void levelChange(const std::vector<int> vector);
    void statsChange();
    void spittyVictory();
    void luckyManager();
};

#endif // ZOOGAME_H
