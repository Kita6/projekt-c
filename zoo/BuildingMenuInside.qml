import QtQuick 2.15

Rectangle{
    anchors.centerIn: parent
    width: parent.width-50
    height: parent.height-50
    Rectangle {
        id: buildingMenuBuildBuilding
        width: 200
        height: 200
        color: "white"
        anchors.centerIn: parent

        Image{
            id: visitorBuilding
            source: "../images/IconBuilding.png"
            width: parent.width
            height: parent.height
        }

        ToolTip{
            id: visitorToolTip
            description: "Facility to make your visitors happy\nPrice: $300\nMaintanance: $30\nServices 20 visitors."
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                buildingMenu.visible = false
                if(game.isEmpty() == true){
                    game.saveIntoTile(10, 300)
                } else{
                    isEmptyDialog.open()
                }
            }
            onEntered: {
                visitorToolTip.visible = true
            }
            onExited: {
                visitorToolTip.visible = false
            }
        }
    }
    // tlačítko pro postavení vybehu s emu - silueta
    Rectangle {
        id: buildingMenuBuildEmu
        width: 200
        height: 200
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top

        Image{
            source: "../images/IconEmu.png"
            width: parent.width
            height: parent.height
        }

        ToolTip{
            id: emuToolTip
            description: "Build home for Emus\nPrice: $200\nMaintanance: $20\nBase number of visitors: 3"
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                buildingMenu.visible = false
                if(game.isEmpty() == true){
                    game.saveIntoTile(20, 200)
                } else{
                    isEmptyDialog.open()
                }
            }
            onEntered: {
                emuToolTip.visible = true
            }
            onExited: {
                emuToolTip.visible = false
            }
        }
    }
    // tlačítko pro postavení vybehu s liskami - silueta
    Rectangle {
        id: buildingMenuBuildFox
        width: 250
        height: 250
        color: "white"
        anchors.verticalCenter: parent.verticalCenter
        x: parent.width - buildingMenuBuildFox.width - 150

        Image{
            source: "../images/IconFox.png"
            width: parent.width
            height: parent.height
        }

        ToolTip{
            id: foxToolTip
            visible: false
            description: "Build home for Foxes\nPrice: $400\nMaintanance: $40\nBase number of visitors: 4"
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                buildingMenu.visible = false
                if(game.isEmpty() == true){
                    game.saveIntoTile(30, 400)
                } else{
                    isEmptyDialog.open()
                }
            }
            onEntered: {
                foxToolTip.visible = true
            }
            onExited: {
                foxToolTip.visible = false
            }
        }
    }
    // tlačítko pro postavení vybehu lamami - silueta
    Rectangle {
        id: buildingMenuBuildLama
        width: 200
        height: 200
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom

        Image{
            source: "../images/IconLama.png"
            width: parent.width
            height: parent.height
        }

        ToolTip{
            id: lamaToolTip
            description: "Build home for THE MOST majestic animals\nPrice: $600\nMaintanance: $60\nBase number of visitors: 5"
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                buildingMenu.visible = false
                if(game.isEmpty() == true){
                    game.saveIntoTile(40, 600)
                } else{
                    isEmptyDialog.open()
                }
            }
            onEntered: {
                lamaToolTip.visible = true
            }
            onExited: {
                lamaToolTip.visible = false
            }
        }
    }
    // tlačítko pro postavení vybehu s okapi - silueta
    Rectangle {
        id: buildingMenuBuildOkapi
        width: 200
        height: 200
        color: "white"
        anchors.verticalCenter: parent.verticalCenter
        x: 200

        Image{
            source: "../images/IconOkapi.png"
            width: parent.width
            height: parent.height
        }

        ToolTip{
            id: okapiToolTip
            description: "Build home for Okapis\nPrice: $800\nMaintanance: $80\nBase number of visitors: 6"
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                buildingMenu.visible = false
                if(game.isEmpty() == true){
                    game.saveIntoTile(50, 800)
                } else{
                    isEmptyDialog.open()
                }
            }
            onEntered: {
                okapiToolTip.visible = true
            }
            onExited: {
                okapiToolTip.visible = false
            }
        }
    }
}
