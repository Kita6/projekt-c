import QtQuick 2.0

//Pozadí hrací plochy
Rectangle {
    width: parent.width
    height: parent.height
    Image {
        id: zooPlay
        source: "../images/ZOO non flag.png"
        anchors.centerIn: parent
        width: parent.width
        height: parent.height
    }

    //Pozadí jednotlivých tiles pro zlepšení viditelnosti
    Rectangle {
        id:plot
        height: 900
        width: 900
        anchors.centerIn: parent
        color: "green"
        border.color: "gray"
        border.width: 20
    }
}
