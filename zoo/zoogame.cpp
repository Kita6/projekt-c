#include "zoogame.h"

ZooGame::ZooGame(QObject *parent) : QObject(parent){
    m_zoo = {0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,};
    m_levels = {0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,};
    m_money = 100000;
    m_turns = 1;
    m_actionsLeft = 3;
    m_goldPerTurn = 0;
    m_zoo.at(0) = 10;
    m_visitors = 0;
    m_maxVisitors = 0;
    m_unhappyTurns = 0;
}

int ZooGame::getZooCount(int index){
    return m_zoo.at(index);
}

int ZooGame::getLevel(int index){
    return m_levels.at(index);
}

void ZooGame::saveIntoTile(int id, int price){
    if (m_actionsLeft > 0) {
        if (m_money >= price){
            qInfo() << "Vector:";
            if (m_zoo[m_index] != 0){
                qInfo() << "Can't build this. Different building already in place.";
            } else {
                m_zoo[m_index] = id;
                m_money = m_money - price;
                for (int i = 0; i < m_zoo.size(); i++) {
                       qInfo() << m_zoo.at(i) << ' ';
                   }
                qInfo() << "Konec vectoru.";
            }
            m_levels[m_index] = 1;
            emit emit vectorChange(m_zoo);
            m_actionsLeft--;
            emit emit topChange();
            emit emit levelChange(m_levels);
            emit emit statsChange();
        }
    }
}

void ZooGame::upgradeLevel(){
    if (m_actionsLeft > 0){
        if(m_money >= 300){
            if (m_levels[m_index] < 5){
                m_levels[m_index]++;
                m_actionsLeft--;
                m_money = m_money - 300;
                if (m_zoo[m_index] > 9 and m_zoo[m_index] < 20) {
                    m_zoo[m_index]++;
                }
            } else {
                qInfo() << "Can't upgrade anymore!";
            }
        } else {
            qInfo() << "Not enough money!";
        }
    }
    emit emit levelChange(m_levels);
    emit emit vectorChange(m_zoo);
    emit emit topChange();
    emit emit statsChange();
}

int ZooGame::getLevel(){
    int m_currentLevel = m_levels[m_index];
    return m_currentLevel;
}

void ZooGame::getIndex(int index){
    m_index = index;
    qInfo() << m_index;
}

void ZooGame::newGame(){
    m_zoo[0] = 1;
    qInfo() << "Vector:";
    for (int i = 0; i < m_zoo.size(); i++) {
           qInfo() << m_zoo.at(i) << ' ';
       }
    qInfo() << "Konec vectoru.";
    emit emit vectorChange(m_zoo);
    emit emit topChange();
    emit emit statsChange();
}

void ZooGame::loadGame(){
    FileManager::loadGame(m_zoo, m_levels, m_money, m_actionsLeft, m_turns);
    qInfo() << "Vector:";
    for (int i = 0; i < m_zoo.size(); i++) {
           qInfo() << m_zoo.at(i) << ' ';
    }
    qInfo() << "Konec vectoru.";
    qInfo() << m_money;
    emit emit vectorChange(m_zoo);
    emit emit levelChange(m_levels);
    emit emit topChange();
    emit emit statsChange();
}

void ZooGame::saveGame(){
    FileManager::saveXml(m_zoo, m_levels, m_money, m_actionsLeft, m_turns);
}

bool ZooGame::isEmpty(){
    if(m_zoo[m_index] == 0){
        return true;
    } else{
        return false;
    }
}

QString ZooGame::getStats(){
    m_stats = "Money: " + QString::number(m_money) + " Money Per Turn: " +
            QString::number(statCalculator::calculateMoneyPerTurn(m_zoo, m_levels)) + " Visitors: " +
            QString::number(statCalculator::calculateVisitors(m_zoo, m_levels)) + "/" +
            QString::number(statCalculator::calculateMaxVisitors(m_zoo)) + " Actions left: " + QString::number(m_actionsLeft) + " Turn: " + QString::number(m_turns);
            if(statCalculator::calculateHappiness(m_zoo, m_levels) == true){
                m_stats.append(" Visitors: Happy");
            } else{
                m_stats.append(" Visitors: Unhappy");
            }
    return m_stats;
}

QString ZooGame::getMoreStats(){
    QString happiness;
    if(statCalculator::calculateHappiness(m_zoo, m_levels) == true){
        happiness = "Visitors: Happy";
    } else{
        happiness = "Visitors: Unhappy";
        happiness.append("\nReason: Capacity exceeded.");
    }
    int income = statCalculator::calculateAllIncome(m_zoo, m_levels);
    int maintanance = statCalculator::calculateMaintanance(m_zoo);
    QString stats = happiness + "\nTotal Income: " + QString::number(income) + "\nMaintanance: "+ QString::number(maintanance);
    return stats;
}

void ZooGame::nextTurn(){
    m_turns++;
    m_money = m_money + statCalculator::calculateMoneyPerTurn(m_zoo, m_levels);
    m_actionsLeft = 3;
    if(statCalculator::calculateHappiness(m_zoo, m_levels) == false) {
        m_unhappyTurns++;
    } else m_unhappyTurns = 0;
    emit emit vectorChange(m_zoo);
    emit emit levelChange(m_levels);
    emit emit topChange();
    emit emit statsChange();
    if (statCalculator::checkEmuEnding(m_zoo)){
        emit emit vivaLaRevolucion();
    }
    if (statCalculator::checkBrokeEnding(m_money, m_unhappyTurns)){
        emit emit iNeedMoney();
    }
    if (statCalculator::checkLamaEnding(m_zoo)){
        emit emit spittyVictory();
    }
    if (statCalculator::checkVictory(m_levels)){
        emit emit luckyManager();
    }
}

// zakladni get metody:
int ZooGame::getMoney(){
   return m_money;
}

int ZooGame:: getTurns(){
    return m_turns;
}

int ZooGame::getActionsLeft(){
    return m_actionsLeft;
}
