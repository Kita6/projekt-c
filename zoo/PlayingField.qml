import QtQuick 2.0
import QtQuick.Controls 2.15

Rectangle {
    id: map;
    visible: false;
    color: "black"
    width: mainWindow.width
    height: mainWindow.height
    property int tileIndex: 0

    //Pozadí hrací plochy
    PlayingFieldBackground{
    }

    //Dialog aby uživatel věděl, že je hra uložena.
    Dialog {
        id: saveDialog
        modal: true
        standardButtons: Dialog.Ok
        title: "Game Saved Succesfully!"
        width: 200
        height: 100
        anchors.centerIn: parent
    }

    Dialog {
        id: isEmptyDialog
        modal: true
        standardButtons: Dialog.Ok
        title: "No space."
        width: 200
        height: 100
        anchors.centerIn: parent
    }

    Grid {        
        id: test
        Repeater {
            id: test2
            model: 4*4
            ZooTile {
                tileIndex: index
            }
        }
        spacing: 10
        anchors.centerIn: parent
    }
    //Vrchní lišta s informacemi
    TopMenu{
    }

    //Boční pomocná lišta pro menu na stavbu budov
    Rectangle {
        color: "transparent"
        height: parent.height
        width: 1
        states: [
            State {
                name: "menuOpened"
                PropertyChanges {
                    target: buildingMenu
                    visible: true
                }
            }
        ]
        id: addBuilding
        anchors.centerIn: parent
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                addBuilding.state = "addBuildingEnter"
            }

            onExited: {
            }
            onClicked: {
                addBuilding.state = "menuOpened"
            }
        }
        //Otevírací menu s budovami.
        Image {
            id: buildingMenu
            source: "../images/buildingMenu.png"
            anchors.centerIn: parent
            width: mainWindow.width/1.5
            height: mainWindow.height/1.5
            visible: false

            BuildingMenuInside{
            }

            //Tlačitko pro zavření menu
            Rectangle {
                id: buildingMenuCloseButton
                color: "transparent"
                anchors.top: buildingMenu.top
                anchors.right: buildingMenu.right
                width: 100
                height: 100

                states: [
                    State {
                        name: "buildCloseButtonEntered"
                        PropertyChanges {
                            target: closeButtonText
                            color: "red"
                        }
                    },
                    State {
                        name: "buildCloseButtonExited"
                    },
                    State {
                        name: "buildCloseButtonClicked"
                        PropertyChanges {
                            target: addBuilding
                            visible: false
                        }
                    }
                ]

                Text {
                    id: closeButtonText
                    text: "X"
                    color: "black"
                    font.pixelSize: 50
                    anchors.centerIn: parent
                }
                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {
                        buildingMenuCloseButton.state = "buildCloseButtonEntered"
                    }
                    onExited: {
                        buildingMenuCloseButton.state = "buildCloseButtonExited"
                    }
                    onClicked: {
                        buildingMenuCloseButton.state = "closeButtonClicked"
                        buildingMenu.visible = false
                    }
                }
            }
        }
    }
    //Pravá boční lišta s tlačítkem na uložení a další turn
    RightSide{
    }
}
