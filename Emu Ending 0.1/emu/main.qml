import QtQuick 2.15
import QtQuick.Window 2.15
import QtMultimedia 5.15


Window {
    width: 1920
    height: 1080
    visible: true
    title: qsTr("Emu Ending")

    Rectangle {

        width: parent.width
        height: parent.height

        Image {
            id: background
            source: "../images/ZOO non flagBW.png"
            width: parent.width
            height: parent.height
        }

        Text{
            id: greatStoryTelling1
            anchors.centerIn: parent
            text: "For a long time, Emu nation longed for freedom. Yet this freedom seemed impossible..."
            font.pixelSize: 30
            color: "red"
            opacity: 100

            Timer {
                id: timer1
                interval: 5000
                running: true
                onTriggered: {
                    greatStoryTelling1.visible = false
                    greatStoryTelling2.visible = true
                    timer2.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling2
            anchors.centerIn: parent
            text: "Until one day a visitor dropped her phone into their prison."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer2
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling2.visible = false
                    greatStoryTelling3.visible = true
                    timer3.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling3
            anchors.centerIn: parent
            text: "They found an article about how their brave ancestors defeated those 'weird monkeys who called themselves\n
                                                        Australians in a fullout war for survival."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer3
                interval: 7000
                running: false
                onTriggered: {
                    greatStoryTelling3.visible = false
                    greatStoryTelling4.visible = true
                    timer4.running = true
                }
           }
        }

        Text {
            id: greatStoryTelling4
            anchors.centerIn: parent
            text: "It was decided. Emu people in this ZOO will fight for their freedom. For their survival."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer4
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling4.visible = false
                    greatStoryTelling5.visible = true
                    timer5.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling5
            anchors.centerIn: parent
            text: "They began to train their military in secret..."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer5
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling5.visible = false
                    greatStoryTelling6.visible = true
                    timer6.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling6
            anchors.centerIn: parent
            text: "...while simultaneously acquiring weapons needed for this revolution."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer6
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling6.visible = false
                    greatStoryTelling7.visible = true
                    timer7.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling7
            anchors.centerIn: parent
            text: "Until one day, they were ready."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer7
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling7.visible = false
                    greatStoryTelling8.visible = true
                    timer8.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling8
            anchors.centerIn: parent
            text: "They attacked people in the ZOO. It was bloody and effective attack."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer8
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling8.visible = false
                    greatStoryTelling9.visible = true
                    timer9.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling9
            anchors.centerIn: parent
            text: "Emu rebels had control of the ZOO within minutes."
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer9
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling9.visible = false
                    greatStoryTelling10.visible = true
                    timer10.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling10
            anchors.centerIn: parent
            text: "They allowed surviving humans to escape and relay a message to the rest of the world:"
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer10
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling10.visible = false
                    greatStoryTelling11.visible = true
                    timer11.running = true
                }
            }
        }

        Text {
            id: greatStoryTelling11
            anchors.centerIn: parent
            text: "We control this ZOO. We control our lives. We are Emu nation. Do not interfere!"
            font.pixelSize: 30
            color: "red"
            visible: false

            Timer {
                id: timer11
                interval: 5000
                running: false
                onTriggered: {
                    greatStoryTelling11.visible = false
                    greatStoryTelling12.visible = true
                    timer12.running = true
                }
            }
        }

        Rectangle {
            id: greatStoryTelling12
            visible: false
            color: "black"
            width: parent.width
            height: parent.height

            Text {
                anchors.centerIn: parent
                text: "GAME OVER"
                font.pixelSize: 80
                color: "red"

                Timer {
                    id: timer12
                    interval: 2000
                    running: false
                    onTriggered: {
                        greatStoryTelling12.visible = false
                        greatStoryTelling13.visible = true
                    }
                }
            }
        }

        Image {
            id: greatStoryTelling13
            source: "../images/Emu Occupied ZOO.png"
            width: parent.width
            height: parent.height
            visible: false
        }
    }
}
